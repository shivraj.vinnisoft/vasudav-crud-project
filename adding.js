const express = require("express")
const router = express.Router();
const userCollection = require("../model/userSchema")
const bcrypt = require("bcryptjs");
router.addUser = async(req, res)=>{
    const hash = await bcrypt.hash(req.body.password,10)
    try{
        const user ={
            name:req.body.name,
            email:req.body.email,
            password:hash,
            address:req.body.address,
            userType:req.body.userType
        }
        const data = await userCollection.create(user);
        if(data){
            return res.status(200).json({
                message:"Adding the user",
                data: data,
                status:true,
            });
        }else{
            return res.status(400).json({
                message:"bad request",
                status:false,
            });
        }
    }catch(error){
        res.status(400).json({error:"Error in Catch Block"});
    }
}
//get the Data in user///

router.getData = async(req, res)=>{
    try{
      
        const Data = await userCollection.find();
        if(Data){
            return res.status(200).json({
                message:"getting the user data",
                data: Data,
                status:true,
            });
        }else{
            return res.status(400).json({
                message:"something want worng",
                status:false,
            });
        }
    }catch(error){
        res.status(400).json({error:"Error in catch Block"});
    }
}


module.exports = router;